﻿using System;
using System.ServiceModel;

using DataServiceContract;

namespace ProxyLogViewer
{
    /// <summary></summary>
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class DataService : IDataService
    {
        /// <summary></summary>
        private uint _pingCounter = 0;

        /// <summary></summary>
        /// <param name="str"></param>
        public void SendString(string str)
        {
            Console.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString(), str));
        }

        /// <summary></summary>
        public void Ping()
        {
            this._pingCounter++;

            if (this._pingCounter % 600 == 0)
            {
                Console.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString(), this._pingCounter));
            }
        }
    }

    /// <summary></summary>
    internal class Program
    {
        /// <summary></summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var stringGuid = args[0];

            Console.WriteLine(string.Format(@"net.pipe://localhost/{0}", stringGuid));
            Console.WriteLine();

            var serviceHost = new ServiceHost(typeof(DataService), new Uri(string.Format(@"net.pipe://localhost/{0}", stringGuid)));

            serviceHost.AddServiceEndpoint(typeof(IDataService), new NetNamedPipeBinding(), "");

            serviceHost.Open();

            Console.ReadLine();
        }
    }
}