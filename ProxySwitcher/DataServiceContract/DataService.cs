﻿using System;
using System.ServiceModel;

namespace DataServiceContract
{
    /// <summary></summary>
    [ServiceContract]
    public interface IDataService
    {
        /// <summary></summary>
        [OperationContract(IsOneWay = true)]
        void Ping();

        /// <summary></summary>
        /// <param name="str"></param>
        [OperationContract(IsOneWay = true)]
        void SendString(string str);
    }
}