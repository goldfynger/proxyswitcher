﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

using Hardcodet.Wpf.TaskbarNotification;

namespace ProxySwitcher
{
    /// <summary></summary>
    public partial class ProxySwitcherPopup : UserControl
    {
        #region DP

        /// <summary></summary>
        public static readonly DependencyProperty ApplyButtonEnabledProperty = DependencyProperty.Register("ApplyButtonEnabled", typeof(bool?), typeof(ProxySwitcherPopup), new PropertyMetadata(false));

        /// <summary></summary>
        public static readonly DependencyProperty CurrentModeProperty = DependencyProperty.Register("CurrentMode", typeof(Mode?), typeof(ProxySwitcherPopup), new PropertyMetadata(Mode.Normal, OnCurrentModePropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty AddButtonEnabledProperty = DependencyProperty.Register("AddButtonEnabled", typeof(bool?), typeof(ProxySwitcherPopup), new PropertyMetadata(false));


        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnCurrentModePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var popup = d as ProxySwitcherPopup;

            popup.CheckApplyButtonEnabled();
            popup.CheckAddButtonEnabled();
        }

        /// <summary></summary>
        public bool? ApplyButtonEnabled
        {
            get
            {
                return this.GetValue(ApplyButtonEnabledProperty) as bool?;
            }
            set
            {
                this.SetValue(ApplyButtonEnabledProperty, value);
            }
        }


        /// <summary></summary>
        public Mode? CurrentMode
        {
            get
            {
                return this.GetValue(CurrentModeProperty) as Mode?;
            }
            set
            {
                this.SetValue(CurrentModeProperty, value);
            }
        }

        /// <summary></summary>
        public bool? AddButtonEnabled
        {
            get
            {
                return this.GetValue(AddButtonEnabledProperty) as bool?;
            }
            set
            {
                this.SetValue(AddButtonEnabledProperty, value);
            }
        }

        #endregion


        private const int _maxProxyCount = 6;

        /// <summary></summary>
        private readonly double _proxyBlockWidth;
        /// <summary></summary>
        private readonly double _proxyBlockHeight;

        /// <summary></summary>
        private readonly double _actionButtonWidth;
        /// <summary></summary>
        private readonly double _actionButtonHeight;

        /// <summary></summary>
        private readonly Duration _moveDuration = new Duration(new TimeSpan(0, 0, 0, 0, 150));
        
        /// <summary></summary>
        private readonly ProxySwitcherManager _proxySwitcherManager = new ProxySwitcherManager();

        /// <summary></summary>
        private ProxySettingsBlock _proxySettingsBlock;

        /// <summary></summary>
        private bool _settingsBlockCanBeApplied;
        /// <summary></summary>
        private bool _newProxyCanBeAdded;

        /// <summary></summary>
        private readonly Action SetGreenIcon;
        /// <summary></summary>
        private readonly Action SetRedIcon;
        /// <summary></summary>
        private readonly Action<Proxy> SetIconColor;

        /// <summary></summary>
        public ProxySwitcherPopup()
        {
            Stream redIconStream = Application.GetResourceStream(new Uri("pack://application:,,,/ProxySwitcher;component/red16.ico")).Stream;
            Stream greenIconStream = Application.GetResourceStream(new Uri("pack://application:,,,/ProxySwitcher;component/green16.ico")).Stream;

            var redIcon = new System.Drawing.Icon(redIconStream);
            var greenIcon = new System.Drawing.Icon(greenIconStream);

            var notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");

            this.SetRedIcon = () => notifyIcon.Icon = redIcon;
            this.SetGreenIcon = () => notifyIcon.Icon = greenIcon;

            this.SetIconColor = prx =>
            {
                if (prx.ProxyIdentifier == Proxy.DisabledProxyGuid)
                {
                    this.SetRedIcon();
                }
                else
                {
                    this.SetGreenIcon();
                }
            };

            this.SetIconColor(this._proxySwitcherManager.CurrentProxy);


            InitializeComponent();

            this.cbUseThisProxy.IsChecked = this._proxySwitcherManager.UseThisProxyFlag;

            this.cbUseThisProxy.Unchecked += (sender, e) => this._proxySwitcherManager.UseThisProxyFlag = false;
            this.cbUseThisProxy.Checked += (sender, e) => this._proxySwitcherManager.UseThisProxyFlag = true;


            this.cbAutoStart.IsChecked = this._proxySwitcherManager.StartWithSystemFlag;

            this.cbAutoStart.Unchecked += (sender, e) => this._proxySwitcherManager.StartWithSystemFlag = false;
            this.cbAutoStart.Checked += (sender, e) => this._proxySwitcherManager.StartWithSystemFlag = true;

            
            this._proxyBlockWidth = (double)FindResource("ProxyBlockWidth");
            this._proxyBlockHeight = (double)FindResource("ProxyBlockHeight");

            this._actionButtonWidth = (double)FindResource("ActionButtonWidth");
            this._actionButtonHeight = (double)FindResource("ActionButtonHeight");

            
            this.Height = this.GetThisHeight(_maxProxyCount);


            if (this._proxySwitcherManager.ProxyList.Count > _maxProxyCount) throw new ApplicationException("Proxy count more than max proxy count.");

            this.SetNewProxyCanBeAdded(this._proxySwitcherManager.ProxyList.Count < _maxProxyCount);

            
            this.cnvMain.Width = this.GetMainCanvasWidth();
            this.cnvMain.Height = this.GetMainCanvasHeight();


            this._proxySwitcherManager.SelectedProxyChanged += (sender, e) =>
            {
                foreach (var child in cnvMain.Children)
                {
                    var rbProxy = child as ProxyRadioButton;

                    if (rbProxy == null) continue; // If child is not proxy (action button, etc.).

                    var proxy = rbProxy.Proxy;

                    if (proxy.ProxyIdentifier == e.OldProxy.ProxyIdentifier) rbProxy.IsChecked = false;

                    if (proxy.ProxyIdentifier == e.NewProxy.ProxyIdentifier && rbProxy.IsChecked == false) rbProxy.IsChecked = true; // If selected proxy added or removed, i.e. checked programmatically.
                }

                this.SetIconColor(e.NewProxy);
            };
            

            this.InitialPlaceActionButtons();

            this.InitialAddProxiesToView();

             
            this.btnAdd.Click += (sender, e) =>
            {
                if (this.CurrentMode == Mode.Change) return;

                if (this.CurrentMode == Mode.Settings) throw new ApplicationException("Current mode must be normal.");

                this.PrepareAddProxyToView();
            };
            

            btnApply.Click += (sender, e) =>
            {
                if (this.CurrentMode == Mode.Change) return;

                if (this.CurrentMode == Mode.Normal) throw new ApplicationException("Current mode must be settings.");

                if (this._proxySettingsBlock.Proxy == null)
                {
                    this.ApplyAddProxyToView();
                }
                else
                {
                    this.ApplyChangeProxySettingsInView();
                }
            };

            this.btnCancel.Click += (sender, e) =>
            {
                if (this.CurrentMode == Mode.Change) return;

                if (this.CurrentMode == Mode.Normal) throw new ApplicationException("Current mode must be settings.");

                if (this._proxySettingsBlock.Proxy == null)
                {
                    this.CancelAddProxyToView();
                }
                else
                {
                    this.CancelChangeProxySettingsInView();
                }
            };
        }
        

        #region Initializations

        /// <summary></summary>
        private void InitialPlaceActionButtons()
        {
            const int addButtonRightIndex = 0;
            const int autoStartCheckBoxRightIndex = 1;

            const int cancelButtonRightIndex = 0;
            const int applyButtonRightIndex = 1;
            const int useThisProxyCheckBoxIndex = 2;


            this.cbAutoStart.Width = GetFullWidthActionButtonWidth(autoStartCheckBoxRightIndex);
            this.cbUseThisProxy.Width = GetFullWidthActionButtonWidth(useThisProxyCheckBoxIndex);            

            var addButtonPoint = this.GetActionButtonTopLeft(addButtonRightIndex, false);
            var autoStartCheckBoxPoint = this.GetActionButtonTopLeft(autoStartCheckBoxRightIndex, true);

            var cancelButtonPoint = this.GetActionButtonTopLeft(cancelButtonRightIndex, false);
            var applyButtonPoint = this.GetActionButtonTopLeft(applyButtonRightIndex, false);
            var useThisProxyCheckBox = this.GetActionButtonTopLeft(useThisProxyCheckBoxIndex, true);


            Canvas.SetTop(this.btnAdd, addButtonPoint.Y);
            Canvas.SetLeft(this.btnAdd, addButtonPoint.X);

            Canvas.SetTop(this.cbAutoStart, autoStartCheckBoxPoint.Y);
            Canvas.SetLeft(this.cbAutoStart, autoStartCheckBoxPoint.X);


            Canvas.SetTop(this.btnCancel, cancelButtonPoint.Y);
            Canvas.SetLeft(this.btnCancel, cancelButtonPoint.X + this.cnvMain.Width);

            Canvas.SetTop(this.btnApply, applyButtonPoint.Y);
            Canvas.SetLeft(this.btnApply, applyButtonPoint.X + this.cnvMain.Width);

            Canvas.SetTop(this.cbUseThisProxy, useThisProxyCheckBox.Y);
            Canvas.SetLeft(this.cbUseThisProxy, useThisProxyCheckBox.X + this.cnvMain.Width);
        }

        /// <summary>Add all existing proxies to view.</summary>
        private void InitialAddProxiesToView()
        {
            foreach (var proxy in this._proxySwitcherManager.ProxyList)
            {
                this.CreateProxyRadioButton(new Point(0, this.GetProxyBlockTop(proxy)), proxy);
            }
        }

        #endregion


        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                if (Keyboard.IsKeyDown(Key.V))
                {
                    try
                    {
                        Process.Start(string.Format("{0}\\ProxyViewer.exe", AppDomain.CurrentDomain.BaseDirectory));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Cannot start ProxyViewer.exe", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else if (Keyboard.IsKeyDown(Key.L))
                {
                    try
                    {
                        this._proxySwitcherManager.StartNamedPipeLogger();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Cannot start named pipe logger.", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }


        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRemoveButtonClick(object sender, EventArgs e)
        {
            if (this.CurrentMode == Mode.Change) return;

            if (this.CurrentMode == Mode.Settings) throw new ApplicationException("Current mode must be normal.");

            this.RemoveProxyFromView((sender as ProxyRadioButton).Proxy);
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSettingsButtonClick(object sender, EventArgs e)
        {
            if (this.CurrentMode == Mode.Change) return;

            if (this.CurrentMode == Mode.Settings) throw new ApplicationException("Current mode must be normal.");

            this.PrepareChangeProxySettingsInView(sender as ProxyRadioButton);
        }


        /// <summary></summary>
        private void PrepareAddProxyToView()
        {
            this.ExtendMainCanvas(() => this.ShowSettings());
        }

        /// <summary></summary>
        private void ApplyAddProxyToView()
        {
            this._proxySettingsBlock.ApplyChanges();

            var proxy = this._proxySettingsBlock.Proxy;

            this._proxySwitcherManager.AddProxy(proxy, this.cbUseThisProxy.IsChecked == true);

            this.CreateProxyRadioButton(new Point(this.cnvMain.Width, this.GetProxyBlockTop(proxy)), proxy);

            this.HideSettings();

            this.SetNewProxyCanBeAdded(this._proxySwitcherManager.ProxyList.Count < _maxProxyCount);
        }

        /// <summary></summary>
        private void CancelAddProxyToView()
        {
            this.HideSettings(() => this.CollapseMainCanvas());
        }


        /// <summary></summary>
        /// <param name="rbProxy"></param>
        private void PrepareChangeProxySettingsInView(ProxyRadioButton rbProxy)
        {
            this.ShowSettings(rbProxy);
        }

        /// <summary></summary>
        private void ApplyChangeProxySettingsInView()
        {
            this._proxySettingsBlock.ApplyChanges();

            this.HideSettings();
        }

        /// <summary></summary>
        private void CancelChangeProxySettingsInView()
        {
            this.HideSettings();
        }


        /// <summary></summary>
        /// <param name="proxy"></param>
        private void RemoveProxyFromView(Proxy proxy)
        {
            var index = this.FindProxyIndex(proxy);

            this._proxySwitcherManager.RemoveProxy(proxy);

            this.RemoveProxyRadioButton(proxy);

            this.CollapseMainCanvas(index);

            this.SetNewProxyCanBeAdded(this._proxySwitcherManager.ProxyList.Count < _maxProxyCount);
        }
        

        #region Dimension functions

        /// <summary></summary>
        /// <returns></returns>
        private double GetMainCanvasWidth()
        {
            return this._proxyBlockWidth + 2;
        }


        /// <summary></summary>
        /// <returns></returns>
        private double GetMainCanvasHeight()
        {
            return GetMainCanvasHeight(this._proxySwitcherManager.ProxyList.Count);
        }

        /// <summary></summary>
        /// <param name="proxyCount"></param>
        /// <returns></returns>
        private double GetMainCanvasHeight(int proxyCount)
        {
            return (this._proxyBlockHeight + 1) * proxyCount + 1 + this._actionButtonHeight + 7;
        }

        /// <summary></summary>
        /// <param name="maxProxyCount"></param>
        /// <returns></returns>
        private double GetThisHeight(int maxProxyCount)
        {
            return this.GetMainCanvasHeight(maxProxyCount);
        }


        /// <summary></summary>
        /// <param name="proxy"></param>
        /// <returns></returns>
        private double GetProxyBlockTop(Proxy proxy)
        {
            return this.GetProxyBlockTop(this.FindProxyIndex(proxy));
        }

        /// <summary></summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private double GetProxyBlockTop(int index)
        {
            return index * (this._proxyBlockHeight + 1);
        }

        /// <summary></summary>
        /// <param name="buttonFromRightIndex"></param>
        /// <param name="fullWidth"></param>
        /// <returns></returns>
        private Point GetActionButtonTopLeft(int buttonFromRightIndex, bool fullWidth)
        {
            var top = this.cnvMain.Height - this._actionButtonHeight - 5;
            var left = fullWidth ? 3 : this.cnvMain.Width - ((this._actionButtonWidth + 1) * (buttonFromRightIndex + 1)) - 2;

            return new Point(left, top);
        }

        /// <summary></summary>
        /// <param name="buttonFromRightIndex"></param>
        /// <returns></returns>
        private double GetFullWidthActionButtonWidth(int buttonFromRightIndex)
        {
            return this.cnvMain.Width - ((this._actionButtonWidth + 1) * buttonFromRightIndex) - 6;
        }

        #endregion

        
        #region Move functions
        
        /// <summary>Add place in bottom of another proxies and in top of action buttons.</summary>
        /// <param name="storyBoardCompleteAction">Runs when the animation complete.</param>
        private void ExtendMainCanvas(Action storyBoardCompleteAction)
        {
            if (storyBoardCompleteAction == null) throw new ArgumentNullException("storyBoardCompleteAction");

            this.CurrentMode = Mode.Change;

            var oldCanvasHeight = this.cnvMain.Height;
            var newCanvasHeight = oldCanvasHeight + this._proxyBlockHeight + 1;
            var offset = newCanvasHeight - oldCanvasHeight;

            Storyboard storyboard = new Storyboard();

            var mainCanvasAnimation = new DoubleAnimation(newCanvasHeight, this._moveDuration);
            Storyboard.SetTarget(mainCanvasAnimation, this.cnvMain);
            Storyboard.SetTargetProperty(mainCanvasAnimation, new PropertyPath("Height"));

            storyboard.Children.Add(mainCanvasAnimation);


            var animationsList = this.CreateActionButtonsVerticalAnimations(offset);

            animationsList.ForEach(a => storyboard.Children.Add(a));


            storyboard.Completed += (sender, e) => storyBoardCompleteAction();

            storyboard.Begin();
        }

        /// <summary>Collapse LAST proxy button.</summary>
        private void CollapseMainCanvas()
        {
            this.CollapseMainCanvas(this._proxySwitcherManager.ProxyList.Count);
        }

        /// <summary>Collapse canvas for one proxy button.</summary>
        /// <param name="collapsedIndex"></param>
        private void CollapseMainCanvas(int collapsedIndex)
        {
            this.CurrentMode = Mode.Change;

            var oldCanvasHeight = this.cnvMain.Height;
            var newCanvasHeight = oldCanvasHeight - this._proxyBlockHeight - 1;
            var offset = newCanvasHeight - oldCanvasHeight;


            Storyboard storyboard = new Storyboard();
            
            var mainCanvasAnimation = new DoubleAnimation(newCanvasHeight, this._moveDuration);
            Storyboard.SetTarget(mainCanvasAnimation, this.cnvMain);
            Storyboard.SetTargetProperty(mainCanvasAnimation, new PropertyPath("Height"));

            storyboard.Children.Add(mainCanvasAnimation);

            var animationsList = this.CreateActionButtonsVerticalAnimations(offset);

            animationsList.ForEach(a => storyboard.Children.Add(a));


            var proxyAnimations = this.CreateProxyButtonsVerticalAnimations(collapsedIndex, offset);

            if (proxyAnimations != null)
            {
                proxyAnimations.ForEach(a => storyboard.Children.Add(a));
            }


            storyboard.Completed += (sender, e) => this.CurrentMode = Mode.Normal; //this.AnimationInProgress = false;


            storyboard.Begin();
        }

        /// <summary>Show settings for existing or new proxy.</summary> 
        private void ShowSettings()
        {
            this.ShowSettings(null);
        }

        /// <summary>Show settings for existing or new proxy.</summary>        
        /// <param name="rbProxy">Can be null if proxy does not exist (new proxy).</param>
        private void ShowSettings(ProxyRadioButton rbProxy)
        {
            this.CurrentMode = Mode.Change;

            Storyboard storyboard = new Storyboard();
            
            if (rbProxy == null) // Proxy does not exist.
            {
                this.CreateProxySettingsBlock(new Point(this.cnvMain.Width, this.GetProxyBlockTop(this._proxySwitcherManager.ProxyList.Count)));
            }
            else
            {
                this.CreateProxySettingsBlock(new Point(this.cnvMain.Width, this.GetProxyBlockTop(rbProxy.Proxy)), rbProxy);

                var proxyBlockAnimation = new DoubleAnimation(this.cnvMain.Width * -1, this._moveDuration);
                Storyboard.SetTarget(proxyBlockAnimation, rbProxy);
                Storyboard.SetTargetProperty(proxyBlockAnimation, new PropertyPath("(Canvas.Left)"));

                storyboard.Children.Add(proxyBlockAnimation);
            }

            storyboard.Children.Add(this.CreateProxySettingsBlockHorizontalAnimation(0));

            var animationsList = this.CreateActionButtonsHorizontalAnimations(this.cnvMain.Width * -1);

            animationsList.ForEach(a => storyboard.Children.Add(a));


            storyboard.Completed += (sender, e) => this.CurrentMode = Mode.Settings; //this.AnimationInProgress = false;

            
            storyboard.Begin();
        }

        /// <summary>Hide settings for existing or new proxy.</summary>
        private void HideSettings()
        {
            this.HideSettings(null);
        }

        /// <summary>Hide settings for existing or new proxy.</summary>
        /// <param name="storyBoardCompleteAction">Can be null if proxy does not exist (new proxy).</param>
        private void HideSettings(Action storyBoardCompleteAction)
        {
            this.CurrentMode = Mode.Change;

            Storyboard storyboard = new Storyboard();

            storyboard.Children.Add(this.CreateProxySettingsBlockHorizontalAnimation(this.cnvMain.Width));

            if (this._proxySettingsBlock.Proxy != null)
            {
                var rbProxy = this.FindProxyRadioButtonOnCanvasForIdentifier(this._proxySettingsBlock.Proxy.ProxyIdentifier);

                var proxyBlockAnimation = new DoubleAnimation(0, this._moveDuration);
                Storyboard.SetTarget(proxyBlockAnimation, rbProxy);
                Storyboard.SetTargetProperty(proxyBlockAnimation, new PropertyPath("(Canvas.Left)"));

                storyboard.Children.Add(proxyBlockAnimation);
            }

            var animationsList = this.CreateActionButtonsHorizontalAnimations(this.cnvMain.Width);

            animationsList.ForEach(a => storyboard.Children.Add(a));


            storyboard.Completed += (sender, e) =>
            {
                this.RemoveCurrentSettingsBlock();

                if (storyBoardCompleteAction != null)
                {
                    storyBoardCompleteAction();
                }
                else
                {
                    this.CurrentMode = Mode.Normal;
                }
            };

            storyboard.Begin();
        }

        #endregion


        #region ActionButtons

        /// <summary></summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        private List<DoubleAnimation> CreateActionButtonsVerticalAnimations(double offset)
        {
            var addButtonAnimation = new DoubleAnimation(Canvas.GetTop(this.btnAdd) + offset, this._moveDuration);
            Storyboard.SetTarget(addButtonAnimation, this.btnAdd);
            Storyboard.SetTargetProperty(addButtonAnimation, new PropertyPath("(Canvas.Top)"));

            var autoStartCheckBoxAnimation = new DoubleAnimation(Canvas.GetTop(this.cbAutoStart) + offset, this._moveDuration);
            Storyboard.SetTarget(autoStartCheckBoxAnimation, this.cbAutoStart);
            Storyboard.SetTargetProperty(autoStartCheckBoxAnimation, new PropertyPath("(Canvas.Top)"));

            var cancelButtonAnimation = new DoubleAnimation(Canvas.GetTop(this.btnCancel) + offset, this._moveDuration);
            Storyboard.SetTarget(cancelButtonAnimation, this.btnCancel);
            Storyboard.SetTargetProperty(cancelButtonAnimation, new PropertyPath("(Canvas.Top)"));

            var applyButtonAnimation = new DoubleAnimation(Canvas.GetTop(this.btnApply) + offset, this._moveDuration);
            Storyboard.SetTarget(applyButtonAnimation, this.btnApply);
            Storyboard.SetTargetProperty(applyButtonAnimation, new PropertyPath("(Canvas.Top)"));

            var useThisProxyCheckBoxAnimation = new DoubleAnimation(Canvas.GetTop(this.cbUseThisProxy) + offset, this._moveDuration);
            Storyboard.SetTarget(useThisProxyCheckBoxAnimation, this.cbUseThisProxy);
            Storyboard.SetTargetProperty(useThisProxyCheckBoxAnimation, new PropertyPath("(Canvas.Top)"));

            return new List<DoubleAnimation> { addButtonAnimation, autoStartCheckBoxAnimation, cancelButtonAnimation, applyButtonAnimation, useThisProxyCheckBoxAnimation };
        }

        /// <summary></summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        private List<DoubleAnimation> CreateActionButtonsHorizontalAnimations(double offset)
        {
            var addButtonAnimation = new DoubleAnimation(Canvas.GetLeft(this.btnAdd) + offset, this._moveDuration);
            Storyboard.SetTarget(addButtonAnimation, this.btnAdd);
            Storyboard.SetTargetProperty(addButtonAnimation, new PropertyPath("(Canvas.Left)"));

            var autoStartCheckBoxAnimation = new DoubleAnimation(Canvas.GetLeft(this.cbAutoStart) + offset, this._moveDuration);
            Storyboard.SetTarget(autoStartCheckBoxAnimation, this.cbAutoStart);
            Storyboard.SetTargetProperty(autoStartCheckBoxAnimation, new PropertyPath("(Canvas.Left)"));

            var cancelButtonAnimation = new DoubleAnimation(Canvas.GetLeft(this.btnCancel) + offset, this._moveDuration);
            Storyboard.SetTarget(cancelButtonAnimation, this.btnCancel);
            Storyboard.SetTargetProperty(cancelButtonAnimation, new PropertyPath("(Canvas.Left)"));

            var applyButtonAnimation = new DoubleAnimation(Canvas.GetLeft(this.btnApply) + offset, this._moveDuration);
            Storyboard.SetTarget(applyButtonAnimation, this.btnApply);
            Storyboard.SetTargetProperty(applyButtonAnimation, new PropertyPath("(Canvas.Left)"));

            var useThisProxyCheckBoxAnimation = new DoubleAnimation(Canvas.GetLeft(this.cbUseThisProxy) + offset, this._moveDuration);
            Storyboard.SetTarget(useThisProxyCheckBoxAnimation, this.cbUseThisProxy);
            Storyboard.SetTargetProperty(useThisProxyCheckBoxAnimation, new PropertyPath("(Canvas.Left)"));

            return new List<DoubleAnimation> { addButtonAnimation, autoStartCheckBoxAnimation, cancelButtonAnimation, applyButtonAnimation, useThisProxyCheckBoxAnimation };
        }

        #endregion


        #region ProxyRadioButton

        /// <summary></summary>
        /// <param name="location"></param>
        /// <param name="proxy"></param>
        private void CreateProxyRadioButton(Point location, Proxy proxy)
        {
            var rbProxy = new ProxyRadioButton(proxy) { IsChecked = this._proxySwitcherManager.IsCurrentProxy(proxy) };

            Canvas.SetLeft(rbProxy, location.X);
            Canvas.SetTop(rbProxy, location.Y);

            rbProxy.PropertyChanged += OnProxyRadioButtonPropertyChanged;

            rbProxy.RemoveClick += this.OnRemoveButtonClick;
            rbProxy.SettingsClick += this.OnSettingsButtonClick;

            rbProxy.KeyDown += this.OnKeyDownHandler;

            this.cnvMain.Children.Add(rbProxy);
        }

        /// <summary></summary>
        /// <param name="proxy"></param>
        private void RemoveProxyRadioButton(Proxy proxy)
        {
            this.RemoveProxyRadioButton(this.FindProxyRadioButtonOnCanvasForIdentifier(proxy.ProxyIdentifier));
        }

        /// <summary></summary>
        /// <param name="rbProxy"></param>
        private void RemoveProxyRadioButton(ProxyRadioButton rbProxy)
        {
            rbProxy.PropertyChanged -= OnProxyRadioButtonPropertyChanged;

            rbProxy.RemoveClick -= this.OnRemoveButtonClick;
            rbProxy.SettingsClick -= this.OnSettingsButtonClick;

            rbProxy.KeyDown -= this.OnKeyDownHandler;

            this.cnvMain.Children.Remove(rbProxy);
        }

        /// <summary>Find ProxyRadioButton on main canvas for identifier.</summary>
        /// <param name="identifier">Proxy guid.</param>
        /// <returns>Founded ProxyRadioButton.</returns>
        /// <exception cref="ApplicationException">If proxy radio button does not exist on canvas.</exception>
        private ProxyRadioButton FindProxyRadioButtonOnCanvasForIdentifier(Guid identifier)
        {
            ProxyRadioButton rbProxy = null;

            foreach (var child in this.cnvMain.Children)
            {
                rbProxy = child as ProxyRadioButton;

                if (rbProxy != null && rbProxy.Proxy.ProxyIdentifier == identifier) break;
            }

            if (rbProxy == null)
            {
                throw new ApplicationException(string.Format("Cannot find ProxyRadioButton on canvas for current identifier: {0}.", identifier));
            }

            return rbProxy;
        }

        /// <summary></summary>
        /// <param name="index"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private List<DoubleAnimation> CreateProxyButtonsVerticalAnimations(int index, double offset)
        {
            var rbProxyList = new List<ProxyRadioButton>(0);

            foreach (var child in this.cnvMain.Children)
            {
                var rbProxy = child as ProxyRadioButton;

                if (rbProxy == null) continue;

                if (this.FindProxyIndex(rbProxy.Proxy) >= index) rbProxyList.Add(rbProxy);
            }

            if (!rbProxyList.Any()) return null;


            var animations = new List<DoubleAnimation>();

            foreach (var rbProxy in rbProxyList)
            {
                var animation = new DoubleAnimation(Canvas.GetTop(rbProxy) + offset, this._moveDuration);
                Storyboard.SetTarget(animation, rbProxy);
                Storyboard.SetTargetProperty(animation, new PropertyPath("(Canvas.Top)"));

                animations.Add(animation);
            }

            return animations;
        }

        /// <summary></summary>
        /// <param name="value"></param>
        private void SetNewProxyCanBeAdded(bool value)
        {
            this._newProxyCanBeAdded = value;

            this.CheckAddButtonEnabled();
        }

        #endregion


        #region ProxySettingsBlock

        /// <summary></summary>
        /// <param name="location">Location on main canvas.</param>
        /// <returns></returns>
        private void CreateProxySettingsBlock(Point location)
        {
            this.CreateProxySettingsBlock(location, null);
        }

        /// <summary></summary>
        /// <param name="location">Location on main canvas.</param>
        /// <param name="rbProxy">Can be null if proxy does not exist (new proxy).</param>
        private void CreateProxySettingsBlock(Point location, ProxyRadioButton rbProxy)
        {
            this._proxySettingsBlock = rbProxy == null ? new ProxySettingsBlock() : new ProxySettingsBlock(rbProxy.Proxy);

            Canvas.SetTop(this._proxySettingsBlock, location.Y);
            Canvas.SetLeft(this._proxySettingsBlock, location.X);

            this.cnvMain.Children.Add(this._proxySettingsBlock);

            this._proxySettingsBlock.PropertyChanged += this.OnProxySettingsBlockPropertyChanged;

            this.SetBlockCanBeApplied(this._proxySettingsBlock.CanBeApplied.Value);

            this._proxySettingsBlock.KeyDown += this.OnKeyDownHandler;
        }

        /// <summary></summary>
        private void RemoveCurrentSettingsBlock()
        {
            if (this._proxySettingsBlock == null) throw new ApplicationException("ProxySettingsBlock does not exist");

            this._proxySettingsBlock.PropertyChanged -= this.OnProxySettingsBlockPropertyChanged;

            this.SetBlockCanBeApplied(false);

            this.cnvMain.Children.Remove(this._proxySettingsBlock);

            this._proxySettingsBlock.KeyDown -= this.OnKeyDownHandler;

            this._proxySettingsBlock = null;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProxySettingsBlockPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case ProxySettingsBlock.PropertyCanBeApplied: this.SetBlockCanBeApplied(this._proxySettingsBlock.CanBeApplied.Value); break;
            }
        }

        /// <summary></summary>
        /// <param name="newXPoint"></param>
        /// <returns></returns>
        private DoubleAnimation CreateProxySettingsBlockHorizontalAnimation(double newXPoint)
        {
            if (this._proxySettingsBlock == null) throw new ApplicationException("ProxySettingsBlock does not exist");

            var proxySettingsBlockAnimation = new DoubleAnimation(newXPoint, this._moveDuration);
            Storyboard.SetTarget(proxySettingsBlockAnimation, this._proxySettingsBlock);
            Storyboard.SetTargetProperty(proxySettingsBlockAnimation, new PropertyPath("(Canvas.Left)"));

            return proxySettingsBlockAnimation;
        }

        /// <summary></summary>
        /// <param name="proxy"></param>
        /// <returns></returns>
        private int FindProxyIndex(Proxy proxy)
        {
            return this._proxySwitcherManager.ProxyList.IndexOf(proxy);
        }

        /// <summary></summary>
        /// <param name="value"></param>
        private void SetBlockCanBeApplied(bool value)
        {
            this._settingsBlockCanBeApplied = value;

            this.CheckApplyButtonEnabled();
        }

        #endregion


        /// <summary></summary>
        private void CheckApplyButtonEnabled()
        {
            this.ApplyButtonEnabled = this._settingsBlockCanBeApplied && this.CurrentMode != Mode.Normal;
        }

        /// <summary></summary>
        private void CheckAddButtonEnabled()
        {
            this.AddButtonEnabled = this._newProxyCanBeAdded && this.CurrentMode != Mode.Settings;
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProxyRadioButtonPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsChecked") return;

            var rbProxy = sender as ProxyRadioButton;

            // Return if button unchecked.
            if (rbProxy.IsChecked != true) return;


            var proxy = rbProxy.Proxy;

            if (!this._proxySwitcherManager.IsCurrentProxy(proxy))
            {
                this._proxySwitcherManager.ChangeSelectedProxy(proxy);
            }
        }
    }

    /// <summary></summary>
    public enum Mode
    {
        /// <summary></summary>
        Normal,
        /// <summary></summary>
        Settings,
        /// <summary></summary>
        Change
    }
}