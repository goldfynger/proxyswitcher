﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;

using DataServiceContract;

namespace ProxySwitcher
{
    /// <summary></summary>
    internal class LogManager
    {
        /// <summary></summary>
        private List<ILogger> _loggers = new List<ILogger>();

        /// <summary></summary>
        public void CreateNewNamedPipeLogger()
        {
            this._loggers.Add(new NamedPipeLogger());
        }

        /// <summary></summary>
        /// <param name="str"></param>
        public void LogString(string str)
        {
            this._loggers.ForEach(l => l.LogString(str));
        }
    }

    /// <summary></summary>
    internal interface ILogger
    {
        /// <summary></summary>
        /// <param name="str"></param>
        void LogString(string str);
    }

    /// <summary></summary>
    internal class NamedPipeLogger : ILogger
    {
        /// <summary></summary>
        private bool _connectionFailed = false;

        /// <summary></summary>
        private ConcurrentQueue<string> _queue = new ConcurrentQueue<string>();

        /// <summary></summary>
        private readonly Guid _guid = Guid.NewGuid();

        /// <summary></summary>
        public NamedPipeLogger()
        {
            //Process.Start("ProxyLogViewer.exe", this._guid.ToString());

            Process.Start(string.Format("{0}\\ProxyLogViewer.exe", AppDomain.CurrentDomain.BaseDirectory), this._guid.ToString());

            ThreadPool.QueueUserWorkItem(new WaitCallback(Connect), null);
        }

        /// <summary></summary>
        /// <param name="ignored"></param>
        private void Connect(object ignored)
        {
            IDataService dataService = null;

            var isConnected = false;
            
            var i = 3;
            while (i-- >= 0)
            {
                try
                {
                    dataService = new ChannelFactory<IDataService>(new NetNamedPipeBinding(), new EndpointAddress(string.Format(@"net.pipe://localhost/{0}", this._guid))).CreateChannel();

                    isConnected = true;
                    break;
                }
                catch (EndpointNotFoundException)
                {
                    Console.WriteLine("EndpointNotFoundException");
                    Console.WriteLine();

                    Thread.Sleep(4000);
                }
            }

            try
            {
                while (isConnected)
                {
                    dataService.Ping();

                    while (!this._queue.IsEmpty)
                    {
                        string str;

                        if (this._queue.TryDequeue(out str))
                        {
                            dataService.SendString(str);
                        }
                    }

                    Thread.Sleep(100);
                }
            }
            catch (CommunicationException)
            {
                this._connectionFailed = true;
            }
        }

        /// <summary></summary>
        /// <param name="str"></param>
        public void LogString(string str)
        {
            if (this._connectionFailed) return;

            this._queue.Enqueue(str);
        }
    }
}