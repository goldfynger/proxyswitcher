﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ProxySwitcher
{
    /// <summary></summary>
    internal sealed partial class ProxyRadioButton : UserControl, INotifyPropertyChanged
    {
        #region DP

        /// <summary></summary>
        public static readonly DependencyProperty ProxyNameProperty = DependencyProperty.Register("ProxyName", typeof(string), typeof(ProxyRadioButton));

        /// <summary></summary>
        public static readonly DependencyProperty ProxyDescriptionProperty = DependencyProperty.Register("ProxyDescription", typeof(string), typeof(ProxyRadioButton));

        /// <summary></summary>
        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsChecked", typeof(bool?), typeof(ProxyRadioButton), new PropertyMetadata(OnIsCheckedPropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool?), typeof(ProxyRadioButton));

        
        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnIsCheckedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var rbProxy = d as ProxyRadioButton;

            rbProxy.OnPropertyChanged("IsChecked");
        }


        /// <summary></summary>
        public string ProxyName
        {
            get
            {
                return this.GetValue(ProxyNameProperty) as string;
            }
            set
            {
                this.SetValue(ProxyNameProperty, value);
            }
        }

        /// <summary></summary>
        public string ProxyDescription
        {
            get
            {
                return this.GetValue(ProxyDescriptionProperty) as string;
            }
            set
            {
                this.SetValue(ProxyDescriptionProperty, value);
            }
        }

        /// <summary></summary>
        public bool? IsChecked
        {
            get
            {
                return this.GetValue(IsCheckedProperty) as bool?;
            }
            set
            {
                this.SetValue(IsCheckedProperty, value);
            }
        }

        /// <summary></summary>
        public bool? IsReadOnly
        {
            get
            {
                return this.GetValue(IsReadOnlyProperty) as bool?;
            }
            set
            {
                this.SetValue(IsReadOnlyProperty, value);
            }
        }        

        #endregion

        /// <summary></summary>
        private Proxy _proxy;

        /// <summary></summary>
        /// <param name="proxy"></param>
        public ProxyRadioButton(Proxy proxy)
        {
            this._proxy = proxy;

            this.ProxyDescription = proxy.ProxyDescription;
            this.ProxyName = proxy.Name;
            this.IsReadOnly = proxy.IsReadonly;

            proxy.PropertyChanged += new PropertyChangedEventHandler(this.OnProxyPropertyChanged);
        }

        /// <summary>Always throw exception.</summary>
        public ProxyRadioButton()
        {
            throw new ApplicationException("Use .ctor ProxyRadioButton(Proxy proxy) instead.");
        }

        /// <summary></summary>
        public event EventHandler<ProxyEventArgs> SettingsClick;
        /// <summary></summary>
        public event EventHandler<ProxyEventArgs> RemoveClick;

        /// <summary></summary>
        public override void OnApplyTemplate()
        {
            var btnSettings = this.Template.FindName("PART_btnSettings", this) as Button;
            var btnRemove = this.Template.FindName("PART_btnRemove", this) as Button;

            btnSettings.Click += (sender, e) =>
                {
                    var handler = this.SettingsClick;
                    if (handler != null) handler(this, new ProxyEventArgs(this._proxy));
                };

            btnRemove.Click += (sender, e) =>
                {
                    var handler = this.RemoveClick;
                    if (handler != null) handler(this, new ProxyEventArgs(this._proxy));
                };

            base.OnApplyTemplate();
        }

        /// <summary></summary>
        public Proxy Proxy { get { return this._proxy; } }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProxyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case Proxy.PropertyName: this.ProxyName = this._proxy.Name; break;

                case Proxy.PropertyProxyDescription: this.ProxyDescription = this._proxy.ProxyDescription; break;
            }
        }

        /// <summary></summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary></summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}