﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using RegistryUtils;

namespace ProxySwitcher
{
    /// <summary></summary>
    internal sealed class ProxySwitcherManager
    {
        /// <summary></summary>
        private List<Proxy> _proxyList;
        /// <summary></summary>
        private Proxy _currentProxy;

        /// <summary></summary>
        private LogManager _logManager = new LogManager();

        /// <summary></summary>
        private bool _useThisProxyFlag;

        /// <summary></summary>
        public ProxySwitcherManager()
        {
            try
            {
                this.LoadData();
            }
            catch
            {
                this._proxyList = new List<Proxy> { Proxy.DisabledProxy };
                this._currentProxy = Proxy.DisabledProxy;
                this._useThisProxyFlag = false;
            }

            this.InitializeRegistryMonitor();
        }

        /// <summary></summary>
        public List<Proxy> ProxyList { get { return this._proxyList; } }

        /// <summary></summary>
        public Proxy CurrentProxy { get { return this._currentProxy; } }

        /// <summary></summary>
        public bool UseThisProxyFlag
        {
            get
            {
                return this._useThisProxyFlag;
            }
            set
            {
                this._useThisProxyFlag = value;

                this.SaveData();
            }
        }

        /// <summary></summary>
        public bool StartWithSystemFlag
        {
            get
            {
                return this.IsProxySwitcherStringSet();
            }
            set
            {
                if (value)
                {
                    this.SetProxySwitcherString();

                    if (!this.IsProxySwitcherStringSet()) throw new InvalidOperationException("Unknown registry error.");
                }
                else
                {
                    this.ClearProxySwitcherString();

                    if (this.IsProxySwitcherStringSet()) throw new InvalidOperationException("Unknown registry error.");
                }
            }
        }

        #region Registry

        /// <summary></summary>
        public event ErrorEventHandler RegistryError;

        /// <summary></summary>
        private void InitializeRegistryMonitor()
        {
            var registryMonitor = new RegistryMonitor(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings");
            registryMonitor.RegChanged += OnRegChanged;
            registryMonitor.Error += OnError;
            registryMonitor.Start();

            this.ChangeRegIfNeed();
        }

        /// <summary></summary>
        /// <param name="e"></param>
        private void RaiseRegistryError(ErrorEventArgs e)
        {
            var handler = this.RegistryError;
            if (handler != null) handler(this, e);
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRegChanged(object sender, EventArgs e)
        {
            var oldValue = string.Format("ProxyEnable: {0}, ProxyServer: {1}", this.GetProxyState(), this.GetProxyString());

            this.ChangeRegIfNeed();

            var newValue = string.Format("ProxyEnable: {0}, ProxyServer: {1}", this.GetProxyState(), this.GetProxyString());

            this._logManager.LogString(string.Format("Registry changed: old value: {0}, new value {1}", oldValue, newValue));
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnError(object sender, ErrorEventArgs e)
        {
            this._logManager.LogString(string.Format("Error: {0}", e.GetException().Message));

            this.RaiseRegistryError(e);
        }

        /// <summary></summary>
        private void ChangeRegIfNeed()
        {
            if (this._currentProxy.IsProxyEnabled != this.GetProxyState())
            {
                if (!this.SetProxyState(this._currentProxy.IsProxyEnabled))
                {
                    this.RaiseRegistryError(new ErrorEventArgs(new InvalidOperationException("Cannot set proxy state.")));
                }
            }

            if (this._currentProxy.ProxyString != this.GetProxyString())
            {
                if (!this.SetProxyString(this._currentProxy.ProxyString))
                {
                    this.RaiseRegistryError(new ErrorEventArgs(new InvalidOperationException("Cannot set proxy string.")));
                }
            }
        }

        /// <summary></summary>
        /// <returns></returns>
        private bool GetProxyState()
        {
            return (int)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyEnable", 0) != 0;
        }

        /// <summary></summary>
        /// <returns></returns>
        private string GetProxyString()
        {
            return (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyServer", string.Empty);
        }

        /// <summary></summary>
        /// <param name="proxyState"></param>
        private bool SetProxyState(bool proxyState)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyEnable", proxyState ? 1 : 0);

            return this.GetProxyState() == proxyState;
        }

        /// <summary></summary>
        /// <param name="proxyString"></param>
        private bool SetProxyString(string proxyString)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyServer", proxyString);

            return this.GetProxyString() == proxyString;
        }

        /// <summary></summary>
        /// <returns></returns>
        private string GetProxySwitcherString()
        {
            return (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "ProxySwitcher", null);
        }

        /// <summary></summary>
        private void SetProxySwitcherString()
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "ProxySwitcher", Assembly.GetEntryAssembly().Location);
        }

        /// <summary></summary>
        private void ClearProxySwitcherString()
        {
            var regKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            regKey.DeleteValue("ProxySwitcher", false);
        }

        /// <summary></summary>
        /// <returns></returns>
        private bool IsProxySwitcherStringSet()
        {
            return this.GetProxySwitcherString() == Assembly.GetEntryAssembly().Location;
        }

        #endregion

        #region Events

        /// <summary></summary>
        public event EventHandler<ProxyChangedEventArgs> SelectedProxyChanged;

        /// <summary></summary>
        /// <param name="oldProxy"></param>
        /// <param name="newProxy"></param>
        private void OnSelectedProxyChanged(Proxy oldProxy, Proxy newProxy)
        {
            var handler = this.SelectedProxyChanged;
            if (handler != null) handler(this, new ProxyChangedEventArgs(oldProxy, newProxy));
        }

        #endregion

        #region Serialization

        /// <summary>Load data and create new proxy list.</summary>
        private void LoadData()
        {
            using (var fileStream = new FileStream(string.Format("{0}\\ProxySwitcher.settings", AppDomain.CurrentDomain.BaseDirectory), FileMode.Open))
            {
                using (var stream = new MemoryStream())
                {
                    fileStream.CopyTo(stream);

                    stream.Position = 0;

                    var serializer = new DataContractJsonSerializer(typeof(SerializationData));

                    var serializationData = (SerializationData)serializer.ReadObject(stream);

                    this._proxyList = new List<Proxy> { Proxy.DisabledProxy };
                    this._proxyList.AddRange(serializationData.ProxyList.Select(p => new Proxy(p)));

                    var currentProxyIdentifier = new Guid(serializationData.CurrentProxyIdentifier);
                    if (currentProxyIdentifier == Proxy.DisabledProxyGuid)
                    {
                        this._currentProxy = Proxy.DisabledProxy;
                    }
                    else
                    {
                        // Throw exception if cannot find proxy for saved guid.
                        this._currentProxy = this._proxyList.First(p => p.ProxyIdentifier == currentProxyIdentifier);
                    }

                    this._useThisProxyFlag = serializationData.UseThisProxyFlag;
                }
            }
        }

        /// <summary>Save data.</summary>
        private void SaveData()
        {
            var serializationData = new SerializationData
            {
                // Serialize all proxies except default (disabled proxy).
                ProxyList = this._proxyList.Where(p => !p.IsReadonly).Select(p => p.GetSerializationProxy()).ToList(),
                CurrentProxyIdentifier = this._currentProxy.ProxyIdentifier.ToString(),
                UseThisProxyFlag = this._useThisProxyFlag
            };

            var serializer = new DataContractJsonSerializer(typeof(SerializationData));

            using (var stream = new MemoryStream())
            {
                serializer.WriteObject(stream, serializationData);

                stream.Position = 0;

                using (var fileStream = new FileStream(string.Format("{0}\\ProxySwitcher.settings", AppDomain.CurrentDomain.BaseDirectory), FileMode.Create))
                {
                    stream.WriteTo(fileStream);
                }
            }            
        }

        #endregion

        #region Current proxy control functions

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProxyStringChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Proxy.PropertyProxyString)
            {
                this.ChangeRegIfNeed();
            }
        }

        /// <summary></summary>
        /// <param name="newProxy"></param>
        public void ChangeSelectedProxy(Proxy newProxy)
        {
            var oldProxy = this._currentProxy;

            oldProxy.PropertyChanged -= this.OnProxyStringChanged;

            this._currentProxy = newProxy;

            newProxy.PropertyChanged += this.OnProxyStringChanged;

            this.SaveData();


            this.ChangeRegIfNeed();

            this.OnSelectedProxyChanged(oldProxy, newProxy);
        }

        /// <summary></summary>
        /// <param name="proxy"></param>
        /// <param name="useThisProxy"></param>
        public void AddProxy(Proxy proxy, bool useThisProxy)
        {
            this._proxyList.Add(proxy);

            this.SaveData();
              

            if (useThisProxy) this.ChangeSelectedProxy(proxy);
        }

        /// <summary></summary>
        /// <param name="proxy"></param>
        public void RemoveProxy(Proxy proxy)
        {
            if (proxy.ProxyIdentifier == this._currentProxy.ProxyIdentifier)
            {
                this.ChangeSelectedProxy(this._proxyList.Where(p => p.ProxyIdentifier == Proxy.DisabledProxyGuid).First());
            }


            this._proxyList.Remove(proxy);

            this.SaveData();
        }

        /// <summary></summary>
        /// <param name="proxy"></param>
        /// <returns></returns>
        public bool IsCurrentProxy(Proxy proxy)
        {
            return proxy.ProxyIdentifier == this._currentProxy.ProxyIdentifier;
        }

        #endregion

        /// <summary></summary>
        public void StartNamedPipeLogger()
        {
            this._logManager.CreateNewNamedPipeLogger();
        }
    }

    #region Proxy

    /// <summary></summary>
    internal sealed class Proxy : INotifyPropertyChanged
    {
        /// <summary></summary>
        public const string PropertyName = "Name";
        /// <summary></summary>
        public const string PropertyProxyString = "ProxyString";
        /// <summary></summary>
        public const string PropertyProxyDescription = "ProxyDescription";

        /// <summary></summary>
        private const string DisabledProxyName = "Proxy disabled";
        /// <summary></summary>
        private const string DisabledProxyDescription = "Without proxy settings";

        /// <summary></summary>
        public static readonly Guid DisabledProxyGuid = new Guid("D97AB055-0CB1-4935-A943-1E37F73EF6BF");
        /// <summary></summary>
        public static readonly Proxy DisabledProxy = new Proxy();

        /// <summary></summary>
        private string _name;
        /// <summary></summary>
        private IPAddress _ipAddress;
        /// <summary></summary>
        private ushort _port;        
        /// <summary></summary>
        private readonly bool _isProxyEnabled;
        /// <summary></summary>
        private string _proxyString;
        /// <summary></summary>
        private string _proxyDescription;
        /// <summary></summary>
        private readonly bool _isReadonly;
        /// <summary></summary>
        private readonly Guid _proxyIdentifier;

        /// <summary></summary>
        /// <param name="serializationProxy"></param>
        public Proxy(SerializationProxy serializationProxy)
        {
            this._name = serializationProxy.Name;
            this._ipAddress = IPAddress.Parse(serializationProxy.IPAddress);
            this._port = serializationProxy.Port;
            this._isProxyEnabled = true;

            this.CreateProxyString();

            this._proxyDescription = this._proxyString;

            this._isReadonly = false;

            this._proxyIdentifier = new Guid(serializationProxy.Identifier);
        }

        /// <summary></summary>
        /// <param name="name"></param>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public Proxy(string name, IPAddress ipAddress, ushort port)
        {
            this._name = name;
            this._ipAddress = ipAddress;
            this._port = port;
            this._isProxyEnabled = true;

            this.CreateProxyString();

            this._proxyDescription = this._proxyString;

            this._isReadonly = false;

            this._proxyIdentifier = Guid.NewGuid();
        }

        /// <summary>For disabled proxy only.</summary>
        private Proxy()
        {
            this._name = DisabledProxyName;
            this._ipAddress = default(IPAddress);
            this._port = default(ushort);
            this._isProxyEnabled = false;

            this._proxyString = string.Empty;

            this._proxyDescription = DisabledProxyDescription;

            this._isReadonly = true;

            this._proxyIdentifier = DisabledProxyGuid;
        }

        /// <summary></summary>
        public string Name { get { return this._name; } }
        /// <summary></summary>
        public bool IsProxyEnabled { get { return this._isProxyEnabled; } }
        /// <summary></summary>
        public string ProxyString { get { return this._proxyString; } }
        /// <summary></summary>
        public string ProxyDescription { get { return this._proxyDescription; } }
        /// <summary></summary>
        public bool IsReadonly { get { return this._isReadonly; } }
        /// <summary></summary>
        public Guid ProxyIdentifier { get { return this._proxyIdentifier; } }

        /// <summary></summary>
        public IPAddress Address { get { return this._ipAddress; } }
        /// <summary></summary>
        public ushort Port { get { return this._port; } }

        /// <summary></summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary></summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary></summary>
        /// <param name="name"></param>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        public void SetNewSettings(string name, IPAddress ipAddress, ushort port)
        {
            if (this._isReadonly) throw new ApplicationException();

            this._name = name;
            this._ipAddress = ipAddress;
            this._port = port;

            this.CreateProxyString();

            this._proxyDescription = this._proxyString;

            this.OnPropertyChanged(PropertyName);
            this.OnPropertyChanged(PropertyProxyString);
            this.OnPropertyChanged(PropertyProxyDescription);
        }

        /// <summary></summary>
        private void CreateProxyString()
        {
            this._proxyString = string.Format("{0}:{1}", this._ipAddress.ToString(), this._port);
        }

        /// <summary></summary>
        /// <returns></returns>
        public SerializationProxy GetSerializationProxy()
        {
            return new SerializationProxy { Name = this._name, IPAddress = this._ipAddress.ToString(), Port = this._port, Identifier = this._proxyIdentifier.ToString() };
        }
    }

    #endregion

    /// <summary></summary>
    [DataContract]
    internal sealed class SerializationProxy
    {
        /// <summary></summary>
        [DataMember]
        public string Name;
        /// <summary></summary>
        [DataMember]
        public string IPAddress;
        /// <summary></summary>
        [DataMember]
        public ushort Port;
        /// <summary></summary>
        [DataMember]
        public string Identifier;
    }

    /// <summary></summary>
    [DataContract]
    internal sealed class SerializationData
    {
        /// <summary></summary>
        [DataMember]
        public List<SerializationProxy> ProxyList;
        /// <summary></summary>
        [DataMember]
        public string CurrentProxyIdentifier;
        /// <summary></summary>
        [DataMember]
        public bool UseThisProxyFlag;
    }

    /// <summary></summary>
    internal sealed class ProxyEventArgs : EventArgs
    {
        /// <summary></summary>
        private Proxy _proxy;

        /// <summary></summary>
        public ProxyEventArgs(Proxy proxy)
        {
            if (proxy == null) throw new ArgumentNullException();

            this._proxy = proxy;
        }

        /// <summary></summary>
        public Proxy Proxy { get { return this._proxy; } }
    }

    /// <summary></summary>
    internal sealed class ProxyChangedEventArgs : EventArgs
    {
        /// <summary></summary>
        private Proxy _oldProxy;
        /// <summary></summary>
        private Proxy _newProxy;

        /// <summary></summary>
        /// <param name="oldProxy"></param>
        /// <param name="newProxy"></param>
        public ProxyChangedEventArgs(Proxy oldProxy, Proxy newProxy)
        {
            if (oldProxy == null) throw new ArgumentNullException();
            if (newProxy == null) throw new ArgumentNullException();

            this._oldProxy = oldProxy;
            this._newProxy = newProxy;
        }

        /// <summary></summary>
        public Proxy OldProxy { get { return this._oldProxy; } }
        /// <summary></summary>
        public Proxy NewProxy { get { return this._newProxy; } }
    }
}