﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace ProxySwitcher
{
    /// <summary></summary>
    internal sealed class ProxySettingsBlock : UserControl, INotifyPropertyChanged
    {
        /// <summary></summary>
        public const string PropertyCanBeApplied = "CanBeApplied";

        #region DP

        /// <summary></summary>
        public static readonly DependencyProperty ProxyNameProperty = DependencyProperty.Register("ProxyName", typeof(string), typeof(ProxySettingsBlock), new PropertyMetadata(OnProxyNamePropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty AddressStringProperty = DependencyProperty.Register("AddressString", typeof(string), typeof(ProxySettingsBlock), new PropertyMetadata(OnAddressStringPropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty PortStringProperty = DependencyProperty.Register("PortString", typeof(string), typeof(ProxySettingsBlock), new PropertyMetadata(OnPortStringPropertyChanged));

        /// <summary></summary>
        public static readonly DependencyProperty CanBeAppliedProperty = DependencyProperty.Register("CanBeApplied", typeof(bool?), typeof(ProxySettingsBlock), new PropertyMetadata(CanBeAppliedPropertyChanged));


        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnProxyNamePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var proxySettingsBlock = d as ProxySettingsBlock;

            proxySettingsBlock.CheckCanBeApplied();
        }

        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnAddressStringPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var proxySettingsBlock = d as ProxySettingsBlock;

            IPAddress address;

            proxySettingsBlock.SetAddress(IPAddress.TryParse(e.NewValue as string, out address) ? address : null);
        }

        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnPortStringPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var proxySettingsBlock = d as ProxySettingsBlock;

            ushort port;

            proxySettingsBlock.SetPort(ushort.TryParse(e.NewValue as string, out port) ? (ushort?)port : null);
        }

        /// <summary></summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void CanBeAppliedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var proxySettingsBlock = d as ProxySettingsBlock;

            proxySettingsBlock.OnPropertyChanged(PropertyCanBeApplied);
        }


        /// <summary></summary>
        public string ProxyName
        {
            get
            {
                return this.GetValue(ProxyNameProperty) as string;
            }
            set
            {
                this.SetValue(ProxyNameProperty, value);
            }
        }

        /// <summary></summary>
        public string AddressString
        {
            get
            {
                return this.GetValue(AddressStringProperty) as string;
            }
            set
            {
                this.SetValue(AddressStringProperty, value);
            }
        }

        /// <summary></summary>
        public string PortString
        {
            get
            {
                return this.GetValue(PortStringProperty) as string;
            }
            set
            {
                this.SetValue(PortStringProperty, value);
            }
        }

        /// <summary></summary>
        public bool? CanBeApplied
        {
            get
            {
                return this.GetValue(CanBeAppliedProperty) as bool?;
            }
            set
            {
                this.SetValue(CanBeAppliedProperty, value);
            }
        } 

        #endregion

        /// <summary></summary>
        private Proxy _proxy = null;
        /// <summary></summary>
        private IPAddress _ipAddress = null;
        /// <summary></summary>
        private ushort? _port = null;

        /// <summary>Used for adding new proxy.</summary>
        public ProxySettingsBlock()
        {
            this.CheckCanBeApplied();
        }

        /// <summary>Used for changing settings of existing proxy.</summary>
        /// <param name="proxy"></param>
        public ProxySettingsBlock(Proxy proxy)
        {
            this._proxy = proxy;

            this.ProxyName = proxy.Name;
            this.AddressString = proxy.Address.ToString();
            this.PortString = proxy.Port.ToString();

            this.CheckCanBeApplied();
        }

        /// <summary></summary>
        public Proxy Proxy { get { return this._proxy; } }

        /// <summary></summary>
        public IPAddress Address { get { return this._ipAddress; } }

        /// <summary></summary>
        public ushort? Port { get { return this._port; } }

        /// <summary></summary>
        /// <param name="address"></param>
        private void SetAddress(IPAddress address)
        {
            this._ipAddress = address;

            this.CheckCanBeApplied();
        }

        /// <summary></summary>
        /// <param name="port"></param>
        private void SetPort(ushort? port)
        {
            this._port = port;

            this.CheckCanBeApplied();
        }

        /// <summary></summary>
        private void CheckCanBeApplied()
        {
            this.CanBeApplied = this._ipAddress != null && this._port != null && this.ProxyName != null;
        }

        /// <summary></summary>
        public void ApplyChanges()
        {
            if (this.CanBeApplied != true) throw new ApplicationException("Changes can not be applied.");

            if (this._proxy == null)
            {
                this._proxy = new Proxy(this.ProxyName, this._ipAddress, this._port.Value);
            }
            else
            {
                this._proxy.SetNewSettings(this.ProxyName, this._ipAddress, this._port.Value);
            }
        }

        /// <summary></summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary></summary>
        /// <param name="propertyName"></param>
        private void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}