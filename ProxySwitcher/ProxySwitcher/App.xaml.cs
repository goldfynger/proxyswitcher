﻿using System.Windows;
using Hardcodet.Wpf.TaskbarNotification;

namespace ProxySwitcher
{
    /// <summary></summary>
    public partial class App : Application
    {
        /// <summary></summary>
        private TaskbarIcon notifyIcon;

        /// <summary></summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            notifyIcon = (TaskbarIcon) FindResource("NotifyIcon");

            notifyIcon.TrayPopup = new ProxySwitcherPopup();
        }

        /// <summary></summary>
        /// <param name="e"></param>
        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose();
            base.OnExit(e);
        }
    }
}