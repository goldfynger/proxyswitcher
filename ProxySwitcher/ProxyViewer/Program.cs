﻿using Microsoft.Win32;
using System;
using System.IO;

using RegistryUtils;

namespace ProxyViewer
{
    /// <summary></summary>
    internal class Program
    {
        /// <summary></summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var registryMonitor = new RegistryMonitor(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings");
            registryMonitor.RegChanged += OnRegChanged;
            registryMonitor.Error += OnError;
            registryMonitor.Start();

            Console.ReadLine();
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnRegChanged(object sender, EventArgs e)
        {
            Console.WriteLine(string.Format("{0}: ProxyEnable: {1}, ProxyServer: {2}", DateTime.Now.ToString(),
                (int)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyEnable", 0),
                (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings", "ProxyServer", string.Empty)));
        }

        /// <summary></summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine(string.Format("Error: {0}",e.GetException().Message));
        }
    }
}