##Instructions

* Extract all *.exe files from archive.
* Run **ProxySwitcher.exe**.
* Click on new icon in tray.
* Click **Add** button.
* Fill fields for new proxy, if need click **Use this proxy**.
* Click **Apply** or **Cancel**.
* Add another proxy if need.

* Existing proxy can be edited or removed.

* **Ctrl+Shift+V** - run **ProxyViewer.exe**.
* **Ctrl+Shift+L** - run **ProxyLogViewer.exe**.

##TODO

1. Sometimes popup in first show displayed incorrect.

2. Debug app's hotkeys works only if one of proxy buttons focused.

3. Icon of checkbox for all toggle buttons.

4. Masks for port and IP address editors.

5. Need add style and template of settings text boxes. - COMPLETED

6. ToolTip colors. - COMPLETED